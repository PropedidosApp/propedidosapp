// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'starter.controllers', 'starter.catalogo', 'starter.cliente', 'satellizer', 'ui.router', 'permission'])

.run(function($ionicPlatform, $rootScope, $auth, $state) {
$rootScope.$on('$stateChangeStart', function(event, toState, fromState){
    if(!$auth.isAuthenticated() && toState.name !== 'login'){
        event.preventDefault();
        $state.go('login');
    }
});

  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})



//Espacio de rutas
.config(function($stateProvider, $urlRouterProvider, $authProvider){

  $authProvider.loginUrl = 'http://costaazul.propedidos.com/auth_login';
  $stateProvider
  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'LoginCtrl',
    controllerAs: 'login'
  })

  .state('selects', {
    url: '/selects/:prevendedor',
    templateUrl: 'templates/selects.html',
    controller: 'SelectsCtrl'
  })

  .state('catalogo', {
    url: '/catalogo/:prevendedor/:cliente',
    templateUrl: 'templates/catalogo.html',
    controller: 'CatalogoCtrl'
  })

  .state('cliente', {
    url: '/selects/:prevendedor/cliente',
    templateUrl: 'templates/cliente.html',
    controller: 'ClienteCtrl'
  })

  //Sigan añadiendo rutas

  $urlRouterProvider.otherwise('/login');
});
