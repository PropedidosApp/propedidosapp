angular.module('starter.cliente', [])
.controller("ClienteCtrl", function($scope, $stateParams, $auth, $rootScope, $ionicHistory, $http, $location, $ionicSideMenuDelegate, $state, $ionicPopup){
    $scope.barrios = [];
    $scope.error;
    $scope.barrio;
    $scope.listCanSwipe = true;

    $scope.init = function() {
        $http({
            url: 'http://costaazul.propedidos.com/barriosapi',
            method: "GET"
        }).success(function(barrios) {
            $scope.barrios = barrios.barrios;
            $scope.currentpage = barrios.current_page;
        });
    };
    $scope.init();

    $scope.myGoBack = function() {
      $location.url("/selects/" + $stateParams.prevendedor);
    };

})
