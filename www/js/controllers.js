angular.module('starter.controllers', [])
.controller("SelectsCtrl", function($scope, $stateParams, $auth, $rootScope, $http, $location, $ionicSideMenuDelegate, $state, $ionicPopup){
    $scope.barrios = [];
    $scope.establecimientos = [];
    $scope.error;
    $scope.barrio;
    $scope.establecimiento;
    $scope.listCanSwipe = true;

    $scope.init = function() {
        $http({
            url: 'http://costaazul.propedidos.com/barriosapi',
            method: "GET"
        }).success(function(barrios) {
            $scope.barrios = barrios.barrios;
            $scope.currentpage = barrios.current_page;
        });
    };
    $scope.init();

    $scope.confirmLogout = function (){
      var myPopup = $ionicPopup.show({
         title: 'Mensaje ProPedidos',
         template: 'Esta seguro que desea <b>Cerrar Sesión</b>',
         buttons: [
           { text: 'No' },
           {
             text: '<b>Si</b>',
             type: 'button-positive',
               onTap: function() {
                 return true;
             }
           },
         ]
       });
       myPopup.then(function(res) {
         if(res) {
             $scope.logout();
           }
       });
    };

    $scope.logout = function() {
    $auth.logout()
        .then(function() {
            // Desconectamos al usuario y lo redirijimos
            $state.go("login")
        });
    };
    $scope.cargarestablecimientos = function() {
        var vm = this;
        $http({
            url: 'http://costaazul.propedidos.com/establecimientosapi',
            method: "GET",
            params: {barrio: vm.select1}
        }).success(function(establecimientos) {
            $scope.establecimientos = establecimientos.establecimientos;
        });
    };

    $scope.datos = function() {
        var vm = this;
        $location.url("/catalogo/" + $stateParams.prevendedor + "/" + vm.select2);

    };

    $scope.datos2 = function() {
        $location.url("/selects/" + $stateParams.prevendedor + "/cliente");

    };

})
.controller("LoginCtrl", function ($auth, $location, $ionicPopup){
    var vm = this;

    this.login = function(){
        $auth.login({
                email: vm.email,
                password: vm.password
            })
            .then(function(){
                // Si se ha logueado correctamente, lo tratamos aquí.
                // Podemos también redirigirle a una ruta
                console.log(vm.email);
                $location.url("/selects/" + vm.email);
            })
            .catch(function(response){
                $ionicPopup.alert({
                    title: 'Error',
                    content: 'Usuario o contraseña incorrectos'
                })
            });
    };
});
