angular.module('starter.catalogo', [])
.controller("CatalogoCtrl", function($scope, $http, $auth, $ionicModal, $ionicPopup, $timeout, $stateParams, $ionicSideMenuDelegate, $state){
  $scope.productos = [];
  $scope.listCanSwipe = true;
  $scope.carrito = [];
  $scope.prioridad = { checked: false };

  $scope.init = function() {
      $http({
          url: 'http://costaazul.propedidos.com/catalogoapi',
          method: "GET"
      }).success(function(productos) {
          $scope.productos = productos.catalogo;
          console.log($scope.productos);
      });
  };
  $scope.init();

  $scope.sendConfirm = function (){
    var confirmar = $ionicPopup.show({
       title: '<h4 class="assertive">Mensaje ProPedidos</h4>',
       template: '<h5>Esta seguro que desea <b>Enviar el Pedido</b></h5>',
       buttons: [
         { text: 'No' },
         {
           text: '<b>Si</b>',
           type: 'button-positive',
             onTap: function() {
               return true;
           }
         },
       ]
     });
     confirmar.then(function(res) {
       if(res) {
           $scope.registrar_pedido();
         }
     });
  };

  $scope.confirmLogout = function (){
    var myPopup = $ionicPopup.show({
       title: '<h4 class="assertive">Mensaje ProPedidos</h4>',
       template: '<h5>Esta seguro que desea <b>Cerrar Sesión</b></h5>',
       buttons: [
         { text: 'No' },
         {
           text: '<b>Si</b>',
           type: 'button-positive',
             onTap: function() {
               return true;
           }
         },
       ]
     });
     myPopup.then(function(res) {
       if(res) {
           $scope.logout();
         }
     });
  };

  $scope.logout = function() {
    $auth.logout()
        .then(function() {
            // Desconectamos al usuario y lo redirijimos
            $state.go("login")
        });
  };


  $scope.toggleLeft = function(){
    $ionicSideMenuDelegate.toggleLeft();
  };

  $scope.shouldShowDelete = false;

  $scope.onItemDelete = function(item) {
    $scope.carrito.splice($scope.carrito.indexOf(item), 1);
  };

  $scope.agregar = function (p) {

      var itemActual;

       for (var i = 0; i < $scope.carrito.length; i++) {
           if ($scope.carrito[i].referencia == p.referencia) {
               itemActual = $scope.carrito[i];
           }
       }

       if (!itemActual) {
           $scope.carrito.push({
               referencia: p.referencia,
               descripcion: p.descripcion,
               imagen: p.imagen,
               Cantidad: 1
           });
       } else {
           itemActual.Cantidad++;
       }

        var myPopup = $ionicPopup.show({
         title: '<h4 class="positive">Mensaje Propedidos</h4>',
         template: '<h5>¡Se ha agregado un producto al carrito!</h5>'
       })

       $timeout(function() {
         myPopup.close(); //close the popup after 3 seconds for some reason
      }, 900);
   };

  /*$scope.registrar_pedido = function() {
        $http({
            url: 'http://localhost:80/propedidos1.0/public/agregarapp',
            method: "POST",
            params: {fk_id_prevendedor: $stateParams.prevendedor, fk_id_cliente: $stateParams.cliente, carrito: $scope.carrito}
        }).success(function(respuesta) {
            console.log(respuesta);
        });
    };*/




    $scope.registrar_pedido = function(){
      $http.post('http://costaazul.propedidos.com/agregarapp', {
         data: {carrito: $scope.carrito, fk_id_prevendedor: $stateParams.prevendedor, fk_id_cliente: $stateParams.cliente, prioridad: $scope.prioridad.checked}
      }).success(function(respuesta){
          console.log(respuesta);
        $scope.carrito = [];
        $scope.prioridad = { checked: false };
        var myPopup = $ionicPopup.alert({
         title: '<h4 class="balanced">Mensaje Propedidos</h4>',
         template: '<h5>PEDIDO REALIZADO CON EXITO</h5>'
       })

       $timeout(function() {
         myPopup.close(); //close the popup after 3 seconds for some reason
      }, 1200);
      }).error(function(){
        if ($scope.carrito == '') {
          var myPopup = $ionicPopup.alert({
           title: '<h4 class="assertive">Mensaje Propedidos</h4>',
           template: '<h5>¡CARRITO VACIO!, por favor agregar productos</h5>'
         })

        }else {
          var myPopup = $ionicPopup.alert({
           title: '<div class="assertive">Mensaje Propedidos</div>',
           template: '¡Error al enviar el pedido!'
         })
      }
      });
   };


    $ionicModal.fromTemplateUrl('templates/modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });
    $scope.openModal = function(carrito) {
        $scope.carrito = carrito;
        $scope.modal.show();
    };
    $scope.closeModal = function() {
        $scope.modal.hide();
    };
    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
        $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hidden', function() {
        // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function() {
        // Execute action
    });
});
